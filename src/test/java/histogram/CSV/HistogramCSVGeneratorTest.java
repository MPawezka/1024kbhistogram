package histogram.CSV;

import histogram.factory.HistogramFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static histogram.config.HistogramConfigurationLoaderTest.TEST_PROPERTIES;
import static org.assertj.core.api.Assertions.assertThat;

public class HistogramCSVGeneratorTest {

    private HistogramCSVGenerator histogramCSVGenerator;
    private HistogramFactory factory;

    @Before
    public void setUp() {
        histogramCSVGenerator = new HistogramCSVGenerator();
        factory = new HistogramFactory(TEST_PROPERTIES);
    }

    @Test
    public void shouldTransformHistogramToCsv() {
        String text = "aabbccd";
        Map<Character, Long> histogram = factory.createHistogram(text);
        String csvHistogram = histogramCSVGenerator.convertHistogramToCSV(histogram);
        assertThat(csvHistogram).isNotEmpty();
        assertThat(csvHistogram).isEqualTo("a,2\nb,2\nc,2\nd,1\n");
    }
}