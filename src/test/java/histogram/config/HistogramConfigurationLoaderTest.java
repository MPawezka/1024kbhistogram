package histogram.config;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class HistogramConfigurationLoaderTest {

    public static final String TEST_PROPERTIES = "src/test/resources/histogram-test.properties";
    private HistogramConfigurationLoader loader;

    @Before
    public void setUp() {
        loader = new HistogramConfigurationLoader();
    }

    @Test
    public void shouldLoadConfigurationCorrectly() {
        HistogramConfiguration config = loader.loadProperties(TEST_PROPERTIES);
        assertThat(config.shouldIgnoreWhiteSpaces()).isTrue();
        assertThat(config.getIgnoreCharacters()).isNotEmpty();
    }
}