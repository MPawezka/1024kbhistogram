package histogram;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Paths;

import static histogram.config.HistogramConfigurationLoaderTest.TEST_PROPERTIES;

public class HistogramFacadeTest {

    private static final String TEST_PROPERTIES_WITH_SPACES = "src/test/resources/histogram-test-with-spaces.properties";
    private HistogramFacade facade;

    @Before
    public void setUp() {
        facade = new HistogramFacade(TEST_PROPERTIES);
    }

    @Test
    public void shouldCreateHistogramAndSaveItToFile() {
        String fileName = "plik.txt";
        facade.saveHistogramToCSV("aabbc", fileName);
        Assertions.assertThat(Paths.get(fileName)).exists();
    }

    @Test
    public void shouldCreateHistogramWithWhiteSpaces() {
        facade = new HistogramFacade(TEST_PROPERTIES_WITH_SPACES);
        String text = "aa bb cc\td\n";
        String histogramCSV = facade.generateHistogramCSV(text);
        System.out.println(histogramCSV);

    }
}