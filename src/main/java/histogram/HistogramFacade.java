package histogram;

import histogram.CSV.HistogramCSVGenerator;
import histogram.factory.HistogramFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class HistogramFacade {

    private HistogramFactory histogramFactory;
    private HistogramCSVGenerator histogramCSVGenerator = new HistogramCSVGenerator();

    public HistogramFacade() {
        this.histogramFactory = new HistogramFactory();
    }

    public HistogramFacade(String propertyFileName) {
        histogramFactory = new HistogramFactory(propertyFileName);
    }

    public Map<Character, Long> generateHistogram(String text) {
        //return histogram from passed text
        return histogramFactory.createHistogram(text);
    }

    public String generateHistogramCSV(String text) {
        //return histogram CSV in String
        return histogramCSVGenerator.convertHistogramToCSV(generateHistogram(text));
    }

    public void saveHistogramToCSV(String text, String fileName) {
        //generate histogram from passed text as argument and save him as CSV to file
        try {
            Files.write(Paths.get(fileName), generateHistogramCSV(text).getBytes());
        } catch (IOException e) {
            System.out.println("Problem when writing to file: " + e.getMessage());
        }
    }
}
