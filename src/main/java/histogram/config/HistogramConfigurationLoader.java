package histogram.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class HistogramConfigurationLoader {

    public static final List<Character> WHITE_SPACES = Arrays.asList(
            '\n', '\t', '\r', ' ');

    public HistogramConfiguration loadProperties(String propertyFileName) {
        //load property from file name

        HistogramConfiguration config = new HistogramConfiguration();

        try {
            List<String> lines = Files.readAllLines(Paths.get(propertyFileName));
            validateProperties(lines);
            boolean shouldIgnoreWhiteSpaces = getShouldIgnoreWhiteSpaces(lines);
            config.setShouldIgnoreWhiteSpaces(shouldIgnoreWhiteSpaces);

            Set<Character> charsToIgnore = getCharsToIgnore(lines);

            if (shouldIgnoreWhiteSpaces) {
                charsToIgnore.addAll(WHITE_SPACES);
            }
            config.setIgnoreCharacters(charsToIgnore);
        } catch (IOException e) {
            System.out.println("Problem reading a file: " + e.getMessage());
        }
        return config;
    }

    private Set<Character> getCharsToIgnore(List<String> lines) {
        return lines.get(1).split("=")[1].chars()
                .mapToObj(c -> (char) c).collect(Collectors.toSet());
    }

    private boolean getShouldIgnoreWhiteSpaces(List<String> lines) {
        String shouldIgnoreWhiteSpacesString = lines.get(0).split("=")[1];
        return Boolean.parseBoolean(shouldIgnoreWhiteSpacesString);
    }

    private void validateProperties(List<String> lines) {
        if (lines.isEmpty() || lines.size() < 2) {
            throw new IllegalStateException("config file is empty or lacking properties");
        }
    }
}
