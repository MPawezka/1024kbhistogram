package histogram.factory;

import histogram.config.HistogramConfiguration;
import histogram.config.HistogramConfigurationLoader;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class HistogramFactory {
    private String propertyFileName = "src/main/resources/histogram.properties";
    private final HistogramConfigurationLoader histogramConfiguration = new HistogramConfigurationLoader();

    public HistogramFactory() {
    }

    public HistogramFactory(String propertyFileName) {
        this.propertyFileName = propertyFileName;
    }

    public Map<Character,Long> createHistogram(String text) {
        //load configuration every time before create histogram
        HistogramConfiguration config = histogramConfiguration.loadProperties(propertyFileName);
        Set<Character> ignoreCharacters = config.getIgnoreCharacters();
        //create histogram, loaded configuration should impacts
        Map<Character, Long> histogram = new TreeMap<>();
        for (char c :text.toLowerCase().toCharArray()){
            addToHistogram(c, histogram, ignoreCharacters);
        }
        return histogram;
    }

    private void addToHistogram(char c, Map<Character, Long> histogram, Set<Character> ignoreCharacters) {
        if (histogram.containsKey(c)) {
            Long currentValue = histogram.get(c);
            histogram.put(c, currentValue+1);
        } else if (isNotIgnoredSign(c, ignoreCharacters)){
            histogram.put(c, 1L);
        }
    }

    private boolean isNotIgnoredSign(char c, Set<Character> ignoreCharacters) {
        return !ignoreCharacters.contains(c);
    }
}
